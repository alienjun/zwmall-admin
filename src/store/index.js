import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
	strict: true,
	state: {
		loginUser: {}
	},
	getters: {
		hasLogin(state) {
			return state.loginUser.token ? true : false;
		},
		token(state) {
			return state.loginUser.token || '';
		}
	},
	mutations: {
		/**
		 * 登录
		 * @param {Object} state
		 * @param {Object} loginUser
		 */
		login(state, loginUser) {
			// 保存登录信息
			state.loginUser = loginUser;
			// 保存登录信息到当前会话
			sessionStorage.setItem('loginUser', JSON.stringify(loginUser));
		},
		/**
		 * 退出登录
		 * @param {Object} state
		 */
		logout(state) {
			// 清空登录信息
			state.loginUser = {};
			// 清除全部缓存
			sessionStorage.clear();
			localStorage.clear();
		}
	}
});